Projet dotfiles
===============


PARTIE UTILISATEURS
-------------------

Prérequis : savoir gérer un dépôt Git

### Explication du projet

Les dotfiles sont des fichiers permettant de configurer des logiciels en fonction de ses préférences.
Ils se nomment "dotfiles" car leur nom commencent par un point, par exemple :
.gitconfig, .vimrc, .bashrc

Une fois ces dotfiles personnalisés, il peut être utile d'avoir un outil permettant de les installer sur une machine, dans le cadre d'un changement d'ordinateur par exemple.

Ce projet sert ainsi à installer de manière simple une liste de dotfiles configurés sur un nouvel ordinateur. 

### Installation du projet

La première étape dans l'installation du projet est le clonage des fichiers de ce projet sur sa machine. Cela est possible en tapant la commande suivante dans le dossier où vous souhaitez mettre le programme :
```
git clone https://gitlab.com/soq-assr-22-23/project-dotfiles.git
```

La deuxième étape est la mise en place d'un dépôt Git contenant les dotfiles devant être installés.

* Cas 1 : Création et initialisation d'un dépôt Git

S'assurer que le dossier /home/user/.config/dtf-store n'existe pas

Utiliser la commande
```
dtf init
```

Cela va créer le dépôt git nommé dtf-store dans le dossier /home/user/.config

* Cas 2 : Clone d'un dépôt Git existant

Utiliser la commande
```
dtf init --clone-remote <URL dépôt git>
``` 

#### Fichier association.json

Il faut créer ou modifier le fichier association.json afin de dire au programme dans quel dossier doit se trouver chaque dotfile.

* Exemple de fichier association.json pour l'association fichier - emplacement
```json
{
  "home": {
    ".zsh_aliases" : ".zsh_aliases"
  },
  "i3": {
        "config" : ".config/i3/config",
        "i3status.conf" : ".config/i3/i3status.conf"
      }
}
```

### Commandes utiles

* Manipulation du dépôt dtf-store

```
dtf git <options git>
```

Cette commande permet d'éviter de se déplacer manuellement dans le dossier dtf-store

* Installation des fichiers avec ou sans backup

```
dtf install <options>

Options:
  --file TEXT             Installe seulement le fichier
  --backup / --no-backup  Crée un backup des fichier avant de les remplacer
                          [default: backup]
  --backup-path TEXT      Défini le chemin de backup  [default:
                          /home/clara/.config/dtf-backup/]
  --help                  Show this message and exit.
```

* Affichage de la liste des dotfiles disponibles

```
dtf show
```

* Affichage des options disponibles

```
dtf <init/git/install/show> --help
```


PARTIE DÉVELOPPEURS
-------------------


### Packages nécessaires

* pytest pour lancer les tests

* typer pour une interface en ligne de commande

### Lancer les tests

Il faut se positionner dans le dossier **tests**
```
pytest -vv --cov-report html --cov=../dtf
```

### Organisation du dépôt
```
.
├── Assignement.md
├── dtf
│   ├── commands
│   │   ├── git.py
│   │   ├── __init__.py
│   │   ├── init.py
│   │   ├── install.py
│   │   ├── __pycache__
│   │   │   ├── git.cpython-310.pyc
│   │   │   ├── __init__.cpython-310.pyc
│   │   │   ├── init.cpython-310.pyc
│   │   │   ├── install.cpython-310.pyc
│   │   │   └── show.cpython-310.pyc
│   │   └── show.py
│   ├── git_command.py
│   ├── helpers.py
│   ├── __init__.py
│   ├── main.py
│   ├── __pycache__
│   │   ├── git_command.cpython-310.pyc
│   │   ├── helpers.cpython-310.pyc
│   │   └── store.cpython-310.pyc
│   └── store.py
├── exemple.association.json
├── pytest.ini
├── README.md
└── tests
    ├── __init__.py
    ├── test_cli.py
    └── test_store.py

```

### Liste des fonctions

* Fichier git.py

`git(command: List[str], store_path)`

command est la liste des commandes git
store_path correspond au dossier /home/user/.config/dtf-store

* Fichier init.py

`init(store_path, clone_remote)`
:   Init a dtf-store repository at the given path

store_path correspond au dossier /home/user/.config/dtf-store
clone_remote correspond à l'adresse du dépôt distant souhaitant être cloné

* Fichier install.py

`backup_dtf(path, store_path, backup_path)`
:   Crée un backup des dotfiles existants localement

path est un dictionnaire associant le dotfile à son emplacement requis
store_path correspond au dossier /home/user/.config/dtf-store
backup_path correspond au chemin où le backup doit être fait
    
`findpath(file, assoc, store_path)`
:   Retourne le chemin où se trouve le dotfile "fichier"

file correspond au nom du fichier
assoc est le fichier association.json
store_path correspond au dossier /home/user/.config/dtf-store

    
`install(path, store_path, backup: bool, backup_path)`
:   Installe les dotfiles sur la machine, avec ou sans backup

path est un dictionnaire associant le dotfile à son emplacement requis
store_path correspond au dossier /home/user/.config/dtf-store
backup est un booléen définissant si un backup est fait ou non
backup_path correspond au chemin où le backup doit être fait

* Fichier show.py

`show(store_path)`
:   show() affiche la liste des dotfiles disponibles dans /home/user/.config/dtf-store


### Workflow

Le workflow choisi pour ce dépôt git est basé sur l'utilisation de différents types de branches :

* La branche prenom-dev

Chaque développeur.se a une branche dédiée sur laquelle iel peut faire les modifications de son choix.

* La branche dev

Elle regroupe les modifications validées par tous.tes les dévelopeur.se.s, chaque branche individuelle est donc constituée du contenu de la branche dev auquel s'ajoutent les modifications récentes.

* La branche master

Elle sert à mettre en production les éléments de la branche dev après validation de tout le monde.
C'est une branche stable contrairement à la branche dev qui n'est pas systématiquement testée.
