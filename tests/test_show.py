import dtf.commands.show
import sys
import os
import helpers

def test_with_wrong_path():
    assert dtf.commands.show.show("wrong_path_because_not_exist") == 0

def test_with_right_path():
    ditfile_lists = """Liste des dotfiles disponibles :\n\nfile1\nfile2\nassociation.json\nfold1/file1\nfold1/file2\n.file\n"""
    path = helpers.extrapolate_bash_variable("./dtf-store_test")
    with open("Output.txt", 'w') as sys.stdout:
        dtf.commands.show.show(path)
    with open("Output.txt", 'r') as f:
        assert ditfile_lists == f.read()
    os.remove("Output.txt")
