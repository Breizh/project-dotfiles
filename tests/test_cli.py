from typer.testing import CliRunner

from dtf.main import app

runner = CliRunner()


def test_app():
    result = runner.invoke(app, ["git", "--help"])
    print(result.stdout)
    assert True

def test_git_exception():
    result = runner.invoke(app, ["git", "nothing"])
    print(result.stdout)
    assert True
