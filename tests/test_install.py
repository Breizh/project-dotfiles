import dtf.commands.install
import os
from datetime import datetime


assoc = "dtf-store_test/association.json"
store_path = "dtf-store_test"
backup_path = "dtf-store-backup_test"

assoc_json = {
  "home": {
    ".file" : ".file",
    "file1" : "file1"
  },
  "fold1": {
    "file1" : "fold1/file1",
    "file2" : "fold1/file2"
      }
}

def test_path_full():
    assert dtf.commands.install.findpath("", assoc, store_path) == assoc_json

def test_path_file1():
    assert dtf.commands.install.findpath("file1", assoc, store_path) == {'file1': 'file1'}

def test_path_fold1_file1():
    assert dtf.commands.install.findpath("fold1/file1", assoc, store_path) == {"fold1/file1" : "fold1/file1"}

def test_path_with_not_assoc_file():
    assert dtf.commands.install.findpath("file2", assoc, store_path) == "error_notassoc"

def test_path_with_not_exist():
    assert dtf.commands.install.findpath("fileX", assoc, store_path) == "error_notfind"

def test_path_with_fold_not_exist():
    assert dtf.commands.install.findpath("foldX/fileX", assoc, store_path) == "error_notfind"

def test_path_with_file_not_exist_in_fold():
    assert dtf.commands.install.findpath("fold1/fileX", assoc, store_path) == "error_notfind"


current_fold = os.getcwd()
home = os.path.expanduser("~")
current_fold = current_fold.replace(home + "/" , "")
store_full_path = current_fold + "/" + store_path

now = datetime.now()
dt_string = now.strftime("%d-%m-%Y_%H-%M-%S")
current_fold = os.getcwd()
backup_full_path = current_fold + "/" + backup_path + "/" + dt_string

assoc_json_full_path = {
  "home": {
    ".file" :store_full_path +  "/.file",
    "file1" : store_full_path + "/file1"
  },
  "fold1": {
    "file1" : store_full_path + "/fold1/file1",
    "file2" : store_full_path + "/fold1/file2"
      }
}


def test_backup():
    dtf.commands.install.backup_dtf(assoc_json_full_path, store_path, backup_path)
    assert os.path.exists(backup_full_path + "/.file")
    assert os.path.exists(backup_full_path + "/file1")
    assert os.path.exists(backup_full_path + "/fold1")
    assert os.path.exists(backup_full_path + "/fold1/file1")
    assert os.path.exists(backup_full_path + "/fold1/file2")

# def test_install():
#     dtf.commands.install.install(assoc_json_full_path, store_path, False, backup_path)
