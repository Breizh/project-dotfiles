import os, tempfile, pathlib
import pytest
import dtf.store


@pytest.fixture
def tempdir():
    temp_dir = tempfile.TemporaryDirectory()
    temp_path = pathlib.Path(temp_dir.name)

    yield temp_path

    temp_dir.cleanup()


def test_store_create(tempdir):
    """
    Test la fonction de création d'un nouveau store
    """
    store_path = "dtf-store"
    test_store = os.path.join(tempdir, store_path)

    dtf.store.create_store(test_store, None)

    assert os.path.exists(test_store)

def test_store_create_with_clone(tempdir):
    """
    Test la fonction de création d'un nouveau store
    """
    store_path = "dtf-store"
    test_store = os.path.join(tempdir, store_path)

    clone_url = "https://gitlab.com/Breizh/dtf-store.git"

    dtf.store.create_store(test_store, clone_url)

    assert os.path.exists(test_store)
