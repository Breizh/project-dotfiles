import subprocess


def extrapolate_bash_variable(string):
    res = subprocess.check_output(f"echo {string}", shell=True)

    # Strip to remove break line and tailing spaces
    return res.strip()
