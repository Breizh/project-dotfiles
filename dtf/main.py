"""
Fichier d'entrée du programme, contient le "main"
"""

import typer
import os
from typing import List, Optional

import commands.init
import commands.git
import commands.show
import commands.install
import helpers

app = typer.Typer(rich_markup_mode="rich")

#Récupération du chemin du home de l'utilisateur
HOME = os.environ['HOME']
# État globale partagé dans toutes les commandes
state = { "store-path": HOME+"/.config/dtf-store/", "assoc-path": HOME+"/.config/dtf-store/association.json",  "backup-path": HOME+"/.config/dtf-backup/" }

@app.command()
def git(command: List[str]):
    """
    Transmet la commande au dépot git contenant les dotfiles

    Exemple:

        dtf git push

    Cette commande pourra être modifié, notamenent pour intégrer une gestion d'erreur. Que ce passe t'il quand le store n'existe pas ? Ou si le store n'est pas un dépot git ?
    """
    store_path = state["store-path"]
    commands.git.git(command, store_path)


@app.command()
def init(clone_remote: str = typer.Option(None, help="Initialise un store de dotfile depuis un dépôt existant.")):
    """
    Initialise un dépot de dotfile si aucun n'est trouvé sur la machine
    Par défaut, le dépot est situé au chemin `$HOME/.config/dtf-store`.

    Le store crée doit être un dépot git.
    """

    store_path = state["store-path"]
    commands.init.init(store_path, clone_remote)


@app.command()
def show():
    """
    Affiche la liste des dotfile disponible.
    """
    store_path = state["store-path"]
    commands.show.show(store_path)


@app.command()
def install(
        file: str = typer.Option("", help="Installe le seulement fichier"),
        backup: bool = typer.Option(True, help="Crée un backup des fichier avant de les remplacer"),
        backup_path: str = typer.Option(state["backup-path"], help="Défini le chemin de backup")
    ):
    """
    Installe les fichiers dotfiles sur la machine courante.
    """

    path = commands.install.findpath(file, state["assoc-path"], state["store-path"])
    if path == "error_notfind":
        print("Le fichier n'existe pas")
    elif path == "error_notassoc":
        print("Le fichier n'est pas associé")
    else:
        commands.install.install(path, state["store-path"], backup, backup_path)


@app.callback()
def main(store_path: str = "$HOME/.config/dtf-store"
        ,assoc_path: str = "$HOME/.config/dtf-store/association.json"
        ,backup_path: str = "$HOME/.config/dtf-backup"
        ):
    """
    Main callback
    """
    # Extrapolate bash variables to get a value for HOME for instance or ~
    state["store-path"] = helpers.extrapolate_bash_variable(store_path)
    state["assoc-path"] = helpers.extrapolate_bash_variable(assoc_path)
    state["backup-path"] = helpers.extrapolate_bash_variable(backup_path)

    # To ease development, use the environment variable DTF-STORE to change the location of the store.
    # So that the dtf-store is located in your dev environment and doesn't pollute your home.
    if os.environ.get("DTF-STORE"):
        state["store-path"] = os.environ["DTF-STORE"]

if __name__ == "__main__":
    app()
