import subprocess, os
import tempfile
import commands.git


def create_store(store_path, clone_remote):
    """
    Create a store.

    First, check if the store already exists.
    """
    if os.path.exists(store_path):
        print("Folder alredy exist")
    else:
        print("Create dotfiles folder")
        os.makedirs(store_path)
        if clone_remote != None:
            print("Clone " + clone_remote + " in dotfiles folder")
            commands.git.git(["clone", "--progress", clone_remote, "."], store_path)
        else:
            print("Init git")
            commands.git.git(["init", "."], store_path)
