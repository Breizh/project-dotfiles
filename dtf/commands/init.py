from store import create_store

def init(store_path, clone_remote):
    """
        Init a dtf-store repository at the given path
    """

    create_store(store_path, clone_remote)
