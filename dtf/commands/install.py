from typing import Optional
import json
import os
from datetime import datetime
import helpers

def file_exist(data, key, file, store_path):
    """
    file_exist() vérifie que l'élement existe dans le tableau d'association

    @param
    data est le tableau d'association
    key est la clé rechercher (soit "home", la racine, soit le nom d'un dossier)
    file est le fichier qu'on recherche
    store_path est le chemin vers le fichier
    """
    # si la clé existe (soit la racine soit un dossier)
    if key in data:
        # si le fichier est bien dans le tableau d'association
        if file in data[key]:
            # si c'est la racine
            if key == "home":
                path = {file : data[key][file]}
            # si c'est un dossier
            else:
                path = {key + "/" + file : data[key][file]}
        # problème détécté
        else:
            # test si le fichier n'existe pas ou si il est mal associé
            liste = os.listdir(store_path)
            if file.encode("utf-8") in liste:
                path = "error_notassoc"
            else:
                path = "error_notfind"
    # problème détécté
    else:
        # test si le fichier n'existe pas ou si il est mal associé
        liste = os.listdir(store_path)
        if key.encode("utf-8") in liste:
            path = "error_notassoc"
        else:
            path = "error_notfind"
    return path

def findpath(file, assoc, store_path):
    """
    findpath() sert à trouver le path  d'un dotfile grace au fichier d'association

    @param
    file est une chaine de caractère contenant le fichier (affiché grace à dtf show), si la chaine est vide c'est tout les fichiers
    assoc est une chaine de caractère contenant le chemin du fichier d'association
    store_path est une chaine de caractère contenant le chemin du dot file store
    """
    # ouverture du json
    with open(assoc, 'r') as f:
        data = json.load(f)

    # si aucun fichier est défini, on renvoie tout les fichers
    if file == "":
        path = data
    else:
        # si ce n'est pas dans un dossier (comme i3/config)
        if len(file.split("/")) < 2:
            # On test que le ficher existe
            path = file_exist(data, "home", file, store_path)
        else:
            # On test que le ficher existe
            split_path = file.split("/", 1)
            path = file_exist(data, split_path[0], split_path[1], store_path)
    return path


def backup_dtf(path, store_path, backup_path):
    """
    backup_dtf() back up les dot files existant du système avant de les remplacer par ceux du dtf_store

    @param
    path est un tableau assiciatif entre la source et la destination du dot file
    store_path le chemin du dtf_store
    backup_path le chemin du dossier de backup
    """
    # si le dossier de back up n'existe pas on le crée
    if not os.path.exists(backup_path):
        os.makedirs(backup_path)
    # on cherche la date et l'heure au format jour-mois-anné_heure-minutes-secode
    now = datetime.now()
    dt_string = now.strftime("%d-%m-%Y_%H-%M-%S")
    # on crée un dossier avec comme nom la date et l'heure
    os.makedirs(backup_path + "/" + dt_string)
    # on itère dans le tableau associatif
    for dir in path.keys():
        # si c'est un ensemble
        if isinstance(path[dir], dict):
            # si ce n'est pas la racine, on crée un dossier (ex. i3/config)
            if dir != "home":
                backup_dir =  backup_path + "/" + dt_string + "/" + dir
                # print(backup_dir)
                os.makedirs(backup_dir)
                for k, v in path[dir].items():
                    # print(k + " : " + v)
                    os.popen("cp " + "$HOME/" + v + " "+ backup_dir + "/" + k)
            else:
                backup_dir = ""

                # Copie des fichiers
                for k, v in path[dir].items():
                    os.popen("cp " + "$HOME/" + v + " "+ backup_path + "/" + dt_string + "/" + backup_dir + k)
        # Copie du fichier
        else:
            os.popen("cp " + "$HOME/" + dir + " "+ backup_path + "/" + dt_string + "/" + path[dir])


def install(path, store_path, backup: bool,  backup_path):
    """
    install() install les dot file au bonne endroit en faisant des liens symbolique

    @param
    path est un tableau assiciatif entre la source et la destination du dot file
    store_path le chemin du dtf_store
    backup est un booleen pour choisir si il y à une back up ou non
    backup_path le chemin du dossier de backup
    """
    # backup des dotfiles
    if backup:
        backup_dtf(path, store_path, backup_path)

    # on itère dans le tableau associatif
    for dir in path.keys():
        # si c'est un ensemble
        if isinstance(path[dir], dict):
            for k, v in path[dir].items():
                # on consruit le path destination
                dst = helpers.extrapolate_bash_variable("$HOME/" + v)
                # si la destination existe déjà on l'annonce
                if os.path.exists(dst):
                    print(k + " alredy exist")
                else:
                    # on construit le path source
                    if dir != "home":
                        src = store_path.decode("utf-8") + "/" + dir + "/" + k
                    else:
                        src = store_path.decode("utf-8") + "/" + k
                    os.symlink(src, dst)
        # si c'est un seul fichier
        else:
            # on consruit le path destination
            dst = helpers.extrapolate_bash_variable("$HOME/" + path[dir])
            # si la destination existe déjà on l'annonce
            if os.path.exists(dst):
                print(dir + " alredy exist")
            else:
                # on construit le path source
                src = store_path.decode("utf-8") + "/" + dir
                os.symlink(src, dst)
