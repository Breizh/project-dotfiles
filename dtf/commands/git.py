from typing import List
from git_command import forward_command

def git(command: List[str], store_path):
    forward_command(command, store_path)
