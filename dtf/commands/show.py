import os

def show(store_path):
    '''
    show() affiche la liste des dotfiles disponibles dans /home/user/.config/dtf-store
    il prend comme argument le chemin du dot file store
    '''

    #On vérifie que le chemin existe
    if not os.path.exists(store_path):
        print("Le dossier", store_path, "n'existe pas")
        return 0
    else:
        print("Liste des dotfiles disponibles :\n")
        liste = os.listdir(store_path)
        for element in liste:
            # Pour enlever le .git
            if element != b".git":
                #on vérifie si l'élément est un fichier
                if os.path.isfile(os.path.join(store_path, element)):
                    print(element.decode("utf-8"))

                #si c'est un dossier, on le parcourt (en considérant qu'il en contient que des fichiers)
                else:
                    for file in os.listdir(os.path.join(store_path,element)):

                        print(element.decode("utf-8")+"/"+file.decode("utf-8"))
        return 1
