import subprocess, os


def forward_command(command, store_path):
    """
    Forward the git command to the store.
    And print its result to the terminal.
    """
    try:
        result = subprocess.check_output(["git"] + command, cwd=store_path)
        print(result.decode())
    except subprocess.CalledProcessError as e:
        print(e.stderr.decode("utf-8"))
